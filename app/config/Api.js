class Api {
  static host = 'http://192.168.43.115:80/winnerhelpdesk/index.php';

  static headers() {
    return {
      Accept: 'application/json',
      'Content-Type': 'application/x-www-form-urlencoded',
    };
  }

  static get(route) {
    return this.xhr(route, null, 'GET');
  }

  static put(route, params) {
    return this.xhr(route, params, 'PUT');  }

  static post(route, params) {
    return this.xhr(route, params, 'POST');
  }

  static delete(route, params) {
    return this.xhr(route, params, 'DELETE');
  }

  static xhr(route, params, verb) {
    const url = `${this.host}/${route}`;

    var formBody = [];
    for (var property in params) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(params[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');

    let options = Object.assign(
      {method: verb},
      verb != 'GET' && params ? {body: formBody} : null,
    );
    options.headers = Api.headers();
    console.log('url', url, options);

    var timeOut = new Promise(function(resolve, reject) {
      setTimeout(
        () =>
          reject({
            status: false,
            status_message: 'Connection Timeout',
            url: url,
            httpStatus: 500,
          }),
          3600000,
      );
    });

    var fetcher = new Promise(function(resolve, reject) {
      fetch(url, options)
        .then(response =>
          response.json().then(data => ({
            url,
            params,
            data,
            status: true,
            httpStatus: response.status,
          })),
        )
        .then(res => {
          resolve(res);
        })
        .catch(function(error) {
          console.log(
            'There has been a problem with your fetch operation: ' + error,
          );
          if (error == 'TypeError: Network request failed') {
            reject({
              status: false,
              status_message: 'Connection Timeout',
              url: url,
              httpStatus: 500,
            });
          } else {
            reject({
              status: false,
              status_message: 'Error Response',
              url: url,
              httpStatus: 500,
            });
          }
        });
    });

    return Promise.race([fetcher, timeOut])
      .then(function(res) {
        console.log('success', res);
        return res;
      })
      .catch(function(error) {
        console.log('error', error);
        return {
          status: false,
          status_message: error.status_message,
          url: url,
          httpStatus: 500,
        };
        throw error;
      });
  }
}

export default Api;
