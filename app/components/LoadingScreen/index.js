import React, { Component } from 'react';
import {Text, View, Button, Image, TextInput, Picker, ScrollView, TouchableOpacity, ToastAndroid, ActivityIndicator} from 'react-native';
import styles from './style';
import {withNavigation} from 'react-navigation';

type Props = {};
export default class LoadingScreen extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
     
    };
  }

  componentDidMount(){
  
  }

  render() {
    return (
        <View style={styles.container}>
            <ActivityIndicator size="large" color="pink" />
        </View>
    );
  }
}