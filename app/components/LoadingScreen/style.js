import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
      backgroundColor: 'rgba(0,0,0,.2)',
      position: 'absolute',
      left: 0,
      top: 0,
      right: 0,
      bottom: 0,
      left: 0,
      justifyContent: 'center',
      alignItems: 'center',
    }
});