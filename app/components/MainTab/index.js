/* eslint-disable eqeqeq */
/* eslint-disable react/no-string-refs */
import React from 'react';
import {
  ToastAndroid,
  Text,
  View,
  Image,
  StatusBar,
  TextInput,
  SafeAreaView,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView
} from 'react-native';

import DeviceInfo from 'react-native-device-info';

// or ES6+ destructured imports

import { getUniqueId, getManufacturer } from 'react-native-device-info';

import Icon from 'react-native-vector-icons/FontAwesome5';
import styles from './style';
import Api from '../../config/Api';

import LoadingScreen from '../../components/LoadingScreen'

import AsyncStorage from '@react-native-community/async-storage';

import { firebase } from '@react-native-firebase/messaging';

import {withNavigation} from 'react-navigation';
import { thisExpression } from '@babel/types';

type Props = {};

class MainTab extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }



  render() {
      console.log(this.props.navigation.state.index)
      let activeTab = this.props.navigation.state.index
    return (
        <View style={styles.container}>
            <TouchableOpacity style={styles.menu}
            onPress={() => this.props.navigation.navigate('Profile')}
            >
            <Icon name="user-alt" size={20} color={ activeTab == 0 ? '#00bfff' : "#a9a9a9" } />
            <Text style={{color:  activeTab == 0 ? '#00bfff' : "#a9a9a9" }}>Profile</Text>
            </TouchableOpacity>


            <TouchableOpacity style={ activeTab == 1 ? styles.menuCircleActive : styles.menuCircle}
            onPress={() => this.props.navigation.navigate('MyTicket')}
            >
            <Icon name="ticket-alt" size={20} color={ activeTab == 1 ? 'white' : "#a9a9a9" } />
            <Text style={{color:  activeTab == 1 ? 'white' : "#a9a9a9" }}>Tickets</Text>
            </TouchableOpacity>

            
            <TouchableOpacity style={styles.menu}
            onPress={() => this.props.navigation.navigate('Logout')}
            >
            <Icon name="power-off" size={20} color={ activeTab == 2 ? '#00bfff' : "#a9a9a9" } />
            <Text style={{color:  activeTab == 2 ? '#00bfff' : "#a9a9a9" }}>Logout</Text>
            </TouchableOpacity>
        </View>
    );
  }
}

export default withNavigation(MainTab);
