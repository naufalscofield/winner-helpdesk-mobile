import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  imageIcon: {
    width: 22,
    height: 22,
    tintColor: 'white',
  },
  mainView: {
    flex: 1,
  },
  titleView: {
    flex: 1,
    backgroundColor: '#00bfff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageTitle: {
    // paddingHorizontal: 40,
    // paddingRight: 50,
    // paddingVertical: 100,
    // paddingTop: 15,
    // paddingBottom: 40,
    marginBottom: 5,
    marginTop: 2,
    // marginLeft: 8,
    // marginRight: 230,
    // justifyContent: 'flex-start',
    width: 104,
    height: 45,
    resizeMode:'contain'
  },
  titleText: {
    fontSize: 25,
    paddingTop: 10,
    color: '#fff',
    marginTop: 0,
    marginBottom: 5,
  },
  formView: {
    alignItems: 'flex-start', 
    // justifyContent: 'left',
    flex: 7,
    backgroundColor: 'white',
    marginLeft: 20,
    marginTop: 30
  },
  warningView: {
    alignItems: 'center', 
    justifyContent: 'center',
    flex: 1,
    backgroundColor: 'white',
    // marginLeft: 20,
    marginTop: 5
  },
  input: {
    justifyContent: 'flex-start',
    borderBottomWidth: 1,
    // textAlign: 'center',
    // borderColor: '#00bfff',
    // borderRadius: 20, 
    paddingTop: 10,
    height: 40,
    width: 350,
    backgroundColor: 'white',
    color: 'black',
    fontSize: 17
  },
  picker: {
    justifyContent: 'flex-start',
    borderBottomWidth: 0.5,
    // borderColor: 'rgb(32, 53, 70)',
    // borderRadius: 20, 
    paddingTop: 10,
    height: 40,
    width: 350,
    // backgroundColor: 'rgb(32, 53, 70)',
    color: 'black',
  },
  label: {
    fontSize: 14, 
    paddingVertical: 7, 
    color: '#00bfff'
  },
  titleWarning: {
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: 18,
    fontWeight:'bold', 
    paddingVertical: 7, 
    color: 'rgb(32, 53, 70)'
  },
  labelWarning: {
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: 15, 
    paddingVertical: 7, 
    color: 'rgb(32, 53, 70)'
  },
  textArea: {
    textAlign: 'left',
    // borderWidth: 2,
    // borderColor: 'rgb(32, 53, 70)',
    // borderRadius: 20,
    borderBottomWidth: 0.5,
    justifyContent: 'flex-start',
    paddingTop: 10,
    // height: 40,
    width: 350,
    backgroundColor: 'white',
    color: 'black',
    fontSize: 15
  },
  buttonView: {
    alignItems: 'center', 
    paddingVertical: 20,
    flex: 2,
    backgroundColor: 'white',
  },
  buttonContainer: {
    backgroundColor: '#f7c744',
    width: 300,
    paddingVertical: 15,
    borderRadius: 20,
  },
  buttonText: {
    textAlign: 'center',
    color: 'rgb(32,53,70)',
    fontWeight: 'bold',
    fontSize: 15,
  },
});
