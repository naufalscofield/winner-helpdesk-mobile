import React from 'react';
import {Text, View, Button, Image, TextInput, Picker, ScrollView, TouchableOpacity, ToastAndroid} from 'react-native';
import styles from './style';
import Api from '../../config/Api';
import LoadingScreen from '../../components/LoadingScreen'
import AsyncStorage from '@react-native-community/async-storage';
import {withNavigation} from 'react-navigation';
import { async } from 'rxjs/internal/scheduler/async';

type Props = {};
class Password extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data:{},
      password:'',
      loading : false
    };
  }

  async _changePassword() {
    let params = {
      password: this.state.password,
      id: this.state.data.id
    };

    this.setState({ loading: true });
    Api.put("API/ChangePass/" + params.id, params)
      .then(async resp => {
        if (resp.httpStatus == 200) {
          this.setState({ loading: false });
          ToastAndroid.show('Update Password Success!', ToastAndroid.SHORT);
          this.setState({ password:'' })
          try {
            await AsyncStorage.setItem(
              "@loginSession",
              JSON.stringify(resp.data.data)
            );
          } catch (e) {
            // saving error
          }
        } else {
          this.setState({ loading: false });
          ToastAndroid.show('Update Password Failed!', ToastAndroid.SHORT);
        }
      })
      .catch(e => {
        console.log(e);
      });
  }

  getData = async () => {
    try {
      const value = await AsyncStorage.getItem("@loginSession");
      if (value !== null) {
        valueFix = JSON.parse(value);
        this.setState({ data: valueFix });
      }
      console.log(value);
    } catch (e) {
      console.log(e);
      // error reading value
    }
  };

  componentDidMount(){
    this.getData();
  }

  render() {
    
    if (this.state.data.password == '3811847612be8eb7cc9238c3861e9ff3') {
    return (
      <ScrollView style={styles.mainView}>
        <View style={styles.mainView}>
          <View style={styles.titleView}>
            <Image
              style={styles.imageTitle}
              source={require('./../../img/logo-winner-warna.png')}
            />
            <Text style={styles.titleText}>Change Password</Text>
          </View>
            <View style={styles.warningView}>
              <Text style={styles.titleWarning}>
              Masih pakai password default?
              </Text>
              <Text style={styles.labelWarning}>
              Segera ganti password anda untuk keamanan.
              </Text>
            </View>
          <View style={styles.formView}>
            <Text style={styles.label}>
              Password Baru Anda
            </Text>
            <TextInput
              value={this.state.password}
              onChangeText={value => this.setState({ password:value })}
              style={styles.input}
              secureTextEntry
              placeholder="Masukan Password Baru Anda"
              placeholderTextColor="lightsteelblue"
              autoCorrect={false}
            />
            
          </View> 
          <View
            style={styles.buttonView}
          >
            <TouchableOpacity
                style={styles.buttonContainer}
                onPress={() => this._changePassword()}
                >
                <Text style={styles.buttonText}>CHANGE PASSWORD</Text>
              </TouchableOpacity>
          </View> 
        </View>
        { this.state.loading && <LoadingScreen /> }
      </ScrollView>
    );
    } else {
      return (
        <ScrollView style={styles.mainView}>
          <View style={styles.mainView}>
            <View style={styles.titleView}>
              <Image
                style={styles.imageTitle}
                source={require('./../../img/logo-winner-warna.png')}
              />
              <Text style={styles.titleText}>Change Password</Text>
            </View>
            <View style={styles.formView}>
              <Text style={styles.label}>
                Password Baru Anda
              </Text>
              <TextInput
                value={this.state.password}
                onChangeText={value => this.setState({ password:value })}
                style={styles.input}
                placeholder="Masukan Password Baru Anda"
                placeholderTextColor="lightsteelblue"
                secureTextEntry
              />
              
            </View> 
            <View
              style={styles.buttonView}
            >
              <TouchableOpacity
                  style={styles.buttonContainer}
                  onPress={() => this._changePassword()}
                  >
                  <Text style={styles.buttonText}>CHANGE PASSWORD</Text>
                </TouchableOpacity>
            </View> 
          </View>
          { this.state.loading && <LoadingScreen /> }
        </ScrollView>
      );
    }
  }
}

export default withNavigation(Password);
