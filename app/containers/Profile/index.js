import React from "react";
import {
  Text,
  View,
  Button,
  Image,
  TextInput,
  Picker,
  ScrollView,
  TouchableOpacity,
  TouchableHighlight,
  ToastAndroid,
  ImageBackground
} from "react-native";
import styles from "./style";
import Api from "../../config/Api";
import LoadingScreen from "../../components/LoadingScreen";
import AsyncStorage from "@react-native-community/async-storage";
import { firebase } from "@react-native-firebase/messaging";
import { withNavigation } from "react-navigation";

type Props = {};
class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      dataBiro: [],
      loading: false,
      selectedBiro: null
    };
  }

  async _updateProfile() {
    let params = {
      nip: this.state.data.nip,
      nm_peg: this.state.data.nm_peg,
      status: this.state.data.status,
      jns_kelamin_peg: this.state.data.jns_kelamin_peg,
      alamat: this.state.data.alamat,
      handphone: this.state.data.handphone,
      email: this.state.data.email,
      kodepos: this.state.data.kodepos,
      telepon: this.state.data.telepon,
      id_biro: this.state.data.id_biro
    };

    this.setState({ loading: true });
    Api.put("API/users/" + params.nip, params)
      .then(async resp => {
        if (resp.httpStatus == 200) {
          this.setState({ loading: false });
          ToastAndroid.show(resp.data.message, ToastAndroid.SHORT);
          try {
            await AsyncStorage.setItem(
              "@loginSession",
              JSON.stringify(resp.data.data)
            );
          } catch (e) {
            // saving error
          }
          this.getData();
        } else {
          this.setState({ loading: false });
          ToastAndroid.show(resp.data.message, ToastAndroid.SHORT);
        }
      })
      .catch(e => {
        console.log(e);
      });
  }

  getData = async () => {
    try {
      const value = await AsyncStorage.getItem("@loginSession");
      if (value !== null) {
        valueFix = JSON.parse(value);
        this.setState({ data: valueFix });
      }
      console.log(value);
    } catch (e) {
      console.log(e);
      // error reading value
    }
  };

  getBiro() {
    Api.get("API/Biro/")
      .then(resp => {
        if (resp.httpStatus == 200) {
          console.log(resp.data.data);
          this.setState({ dataBiro: resp.data.data });
        } else {
          ToastAndroid.show(resp.data.message, ToastAndroid.SHORT);
        }
      })
      .catch(e => {
        console.log(e);
      });
  }

  toPassword() {
    this.props.navigation.navigate('Password');
  }

  async componentDidMount() {
    const hasPermission = await firebase.messaging().hasPermission();
    console.log("tessss", hasPermission);
    this.getData();
    this.getBiro();
  }

  static navigationOptions = {
    tabBarIcon: ({ tintColor }) => (
      <Image
        source={require("./../../img/icon-profile.png")}
        style={styles.imageIcon}
      />
    )
  };
  render() {
    return (
      <View style={{flex:1}}>
          <View style={styles.headerView}>
            <View style={{ flexDirection: "row"}}>
              {/* <ImageBackground
                source={require("./../../img/blue-header.jpg")}
                style={{width: '100%', height: '205.5%'}}
              /> */}
              <Image
                style={styles.imageTitle}
                source={require("./../../img/logo-winner-warna.png")}
              />
              <TouchableOpacity
                onPress={() => this.toPassword()}
              >
                <Image
                  style={styles.imagePassword}
                  source={require("./../../img/key-icon.png")}
                />
              </TouchableOpacity>
            </View>
            <Image
              style={styles.profilePhoto}
              source={require("./../../img/profile-photo.png")}
            />
            <Text style={styles.titleText}>{this.state.data.nm_peg}</Text>
          </View>
      <ScrollView>
        <View style={styles.mainView}>
          <View style={styles.formView}>
            <Text style={styles.label}>NIP</Text>
            <TextInput
              editable={false}
              value={this.state.data.nip}
              onChangeText={value =>
                this.setState({ data: { ...this.state.data, nip: value } })
              }
              style={styles.inputDisable}
              placeholder="NIP anda"
              placeholderTextColor="lightsteelblue"
              returnKeyType="next"
              autoCorrect={false}
              ref={"txtNip"}
            />

            <Text style={styles.label}>Kode Jabatan</Text>
            <TextInput
              editable={false}
              value={this.state.data.kd_jabatan}
              onChangeText={value =>
                this.setState({
                  data: { ...this.state.data, kd_jabatan: value }
                })
              }
              style={styles.inputDisable}
              placeholderTextColor="lightsteelblue"
              returnKeyType="next"
              autoCorrect={false}
            />

            <Text style={styles.label}>Nama Jabatan</Text>
            <TextInput
              editable={false}
              value={this.state.data.nm_jabatan}
              onChangeText={value =>
                this.setState({
                  data: { ...this.state.data, nm_jabatan: value }
                })
              }
              style={styles.textAreaDisable}
              placeholderTextColor="lightsteelblue"
              returnKeyType="next"
              autoCorrect={false}
            />

            <Text style={styles.label}>Kode Unit Org</Text>
            <TextInput
              editable={false}
              value={this.state.data.kd_unit_org}
              onChangeText={value =>
                this.setState({
                  data: { ...this.state.data, kd_unit_org: value }
                })
              }
              style={styles.textAreaDisable}
              placeholderTextColor="lightsteelblue"
              returnKeyType="next"
              autoCorrect={false}
            />

            <Text style={styles.label}>Nama Unit Org</Text>
            <TextInput
              editable={false}
              value={this.state.data.nm_unit_org}
              onChangeText={value =>
                this.setState({
                  data: { ...this.state.data, nm_unit_org: value }
                })
              }
              style={styles.textAreaDisable}
              multiline
              numberOfLines={3}
              placeholderTextColor="lightsteelblue"
              returnKeyType="next"
              autoCorrect={false}
            />

            <Text style={styles.label}>Nama Pegawai</Text>
            <TextInput
              value={this.state.data.nm_peg}
              onChangeText={value =>
                this.setState({ data: { ...this.state.data, nm_peg: value } })
              }
              style={styles.input}
              placeholder="Nama anda"
              placeholderTextColor="lightsteelblue"
              returnKeyType="next"
              autoCorrect={false}
              ref={"txtNmPeg"}
              onSubmitEditing={() => this.refs.txtStatus.focus()}
            />

            <Text style={styles.label}>Status</Text>
            <Picker
              selectedValue={this.state.data.status}
              onValueChange={(itemValue, itemIndex) =>
                this.setState({
                  data: { ...this.state.data, status: itemValue }
                })
              }
              style={styles.picker}
              ref={"txtStatus"}
              onSubmitEditing={() => this.refs.txtJnsKelaminPeg.focus()}
            >
              <Picker.Item label="AKTIF" value="aktif" />
              <Picker.Item label="NONAKTIF" value="nonaktif" />
            </Picker>

            <Text style={styles.label}>Jenis Kelamin</Text>
            <Picker
              selectedValue={this.state.data.jns_kelamin_peg}
              onValueChange={(itemValue, itemIndex) =>
                this.setState({
                  data: { ...this.state.data, jns_kelamin_peg: itemValue }
                })
              }
              style={styles.picker}
              ref={"txtJnsKelaminPeg"}
              onSubmitEditing={() => this.refs.txtAlamat.focus()}
            >
              <Picker.Item label="PRIA" value="PRIA" />
              <Picker.Item label="WANITA" value="WANITA" />
            </Picker>

            <Text style={styles.label}>Alamat</Text>
            <TextInput
              value={this.state.data.alamat}
              onChangeText={value =>
                this.setState({ data: { ...this.state.data, alamat: value } })
              }
              style={styles.textArea}
              multiline
              numberOfLines={3}
              placeholder="Alamat anda"
              placeholderTextColor="lightsteelblue"
              returnKeyType="next"
              autoCorrect={false}
              ref={"txtAlamat"}
              onSubmitEditing={() => this.refs.txtKodepos.focus()}
            />

            <Text style={styles.label}>Kodepos</Text>
            <TextInput
              value={this.state.data.kodepos}
              onChangeText={value =>
                this.setState({ data: { ...this.state.data, kodepos: value } })
              }
              style={styles.input}
              placeholder="Kodepos anda"
              placeholderTextColor="lightsteelblue"
              returnKeyType="next"
              autoCorrect={false}
              ref={"txtKodepos"}
              onSubmitEditing={() => this.refs.txtTelepon.focus()}
            />

            <Text style={styles.label}>Telepon</Text>
            <TextInput
              value={this.state.data.telepon}
              onChangeText={value =>
                this.setState({ data: { ...this.state.data, telepon: value } })
              }
              style={styles.input}
              placeholder="Telepon anda"
              placeholderTextColor="lightsteelblue"
              returnKeyType="next"
              autoCorrect={false}
              ref={"txtTelepon"}
              onSubmitEditing={() => this.refs.txtEmail.focus()}
            />

            <Text style={styles.label}>Email</Text>
            <TextInput
              value={this.state.data.email}
              onChangeText={value =>
                this.setState({ data: { ...this.state.data, email: value } })
              }
              style={styles.input}
              placeholder="Email anda"
              placeholderTextColor="lightsteelblue"
              returnKeyType="next"
              autoCorrect={false}
              ref={"txtEmail"}
              onSubmitEditing={() => this.refs.txtHandphone.focus()}
            />

            <Text style={styles.label}>Handphone</Text>
            <TextInput
              value={this.state.data.handphone}
              onChangeText={value =>
                this.setState({
                  data: { ...this.state.data, handphone: value }
                })
              }
              style={styles.input}
              placeholder="Handphone anda"
              placeholderTextColor="lightsteelblue"
              returnKeyType="next"
              autoCorrect={false}
              ref={"txtHandphone"}
            />

            <Text style={styles.label}>Biro</Text>
            <Picker
              selectedValue={this.state.data.id_biro}
              onValueChange={(itemValue, itemIndex) =>
                this.setState({
                  data: { ...this.state.data, id_biro: itemValue }
                })
              }
              style={styles.picker}
              ref={"txtBiro"}
            >
              {this.state.dataBiro.map((data, index) => {
                return (
                  <Picker.Item
                    label={data.nama_biro}
                    value={data.id}
                    key={"key" + index}
                  />
                );
              })}
            </Picker>
          </View>
          <View style={styles.buttonView}>
            <TouchableOpacity
              style={styles.buttonContainer}
              onPress={() => this._updateProfile()}
            >
              <Text style={styles.buttonText}>UPDATE PROFILE</Text>
            </TouchableOpacity>
          </View>
          {/* <View style={styles.buttonView}>
            <TouchableOpacity
              style={styles.buttonPass}
              onPress={() => this._updateProfile()}
            >
              <Text style={styles.buttonTextPass}>CHANGE PASSWORD</Text>
            </TouchableOpacity>
          </View> */}
        </View>
        {this.state.loading && <LoadingScreen />}
      </ScrollView>
      </View>
    );
  }
}

export default withNavigation(Profile);
