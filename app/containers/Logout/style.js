import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  imageIcon: {
    width: 22,
    height: 22,
    tintColor: 'white',
  },
  mainView: {
    flex: 1,
  },
  titleView: {
    flex: 1,
    backgroundColor: 'rgb(32, 53, 70)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageTitle: {
    paddingTop: 10,
    width: 110,
    height: 40,
  },
  titleText: {
    fontSize: 25,
    paddingTop: 10,
    color: '#fff',
  },
  formView: {
    alignItems: 'center', 
    // justifyContent: 'center',
    flex: 7,
    backgroundColor: 'white',
  },
  input: {
    justifyContent: 'flex-start',
    borderWidth: 2,
    textAlign: 'center',
    borderColor: 'rgb(32, 53, 70)',
    borderRadius: 20, 
    paddingTop: 10,
    height: 40,
    width: 300,
    backgroundColor: 'white',
    color: 'black',
    fontSize: 17
  },
  picker: {
    justifyContent: 'flex-start',
    borderWidth: 2,
    borderColor: 'rgb(32, 53, 70)',
    borderRadius: 20, 
    paddingTop: 10,
    height: 40,
    width: 300,
    backgroundColor: 'rgb(32, 53, 70)',
    color: 'white',
  },
  textArea: {
    borderWidth: 2,
    borderColor: 'rgb(32, 53, 70)',
    borderRadius: 20,
    justifyContent: 'flex-start',
    paddingTop: 10,
    // height: 40,
    width: 300,
    backgroundColor: 'white',
    color: 'black',
    fontSize: 15
  },
  buttonView: {
    alignItems: 'center', 
    paddingVertical: 20,
    flex: 2,
    backgroundColor: 'white',
  },
  buttonContainer: {
    backgroundColor: '#f7c744',
    width: 300,
    paddingVertical: 15,
    borderRadius: 20,
  },
  buttonText: {
    textAlign: 'center',
    color: 'rgb(32,53,70)',
    fontWeight: 'bold',
    fontSize: 15,
  },
});
