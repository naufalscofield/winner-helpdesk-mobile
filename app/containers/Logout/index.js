import React from "react";
import { Image, Alert, View } from "react-native";
import styles from "./style";
import Api from '../../config/Api';
import LoadingScreen from "../../components/LoadingScreen";
import AsyncStorage from "@react-native-community/async-storage";
import { withNavigation, NavigationEvents } from "react-navigation";
import { firebase } from '@react-native-firebase/messaging';
// import { firebase } from '@react-native-firebase/iid';
import { async } from "rxjs/internal/scheduler/async";

type Props = {};
class Logout extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data:{},
    };
  }

  static navigationOptions = {
    tabBarIcon: ({ tintColor }) => (
      <Image
        source={require("./../../img/icon-logout.png")}
        style={styles.imageIcon}
      />
    )
  };

  async _logout() {
    try {
      this._deleteFcmToken()
      // this._deleteDeviceToken()
      await AsyncStorage.removeItem("@loginSession");
      this.props.navigation.replace('Login');
    } catch (exception) {
      return false;
    }
  }

  // async _deleteDeviceToken()
  // {
  //   await firebase.iid().deleteToken(firebase.app().options.storageBucket, '*');
  // }

  async _deleteFcmToken()
  {
    try {
      const value = await AsyncStorage.getItem('@loginSession')
      if(value !== null) {
        valueFix = JSON.parse(value);
          this.setState({data:valueFix});  
      } 
      console.log(value);
    } catch(e) {
      console.log(e);
      // error reading value
    }
    
    let iduser = this.state.data.id
    let params = {
      fcm_token: null
    };
    
    this.setState({loading: true});
    Api.put('API/revokeToken/' + iduser , params)
    .then(async resp => {
      if (resp.httpStatus == 200) {
      }
    })
    .catch(e => {
      console.log(e);
    });
  }

  showAlert() {
    Alert.alert(
      "LOGOUT",
      "Apakah anda yakin?",
      [
        // {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
        {
          text: "Cancel",
          onPress: () => this.props.navigation.goBack(),
          style: "cancel"
        },
        { text: "OK", onPress: () => this._logout() }
      ],
      { cancelable: false }
    );
  }

  render() {
    return (
      <View>
        <NavigationEvents onDidFocus={payload => this.showAlert()} />
      </View>
    );
  }
}

export default withNavigation(Logout);
