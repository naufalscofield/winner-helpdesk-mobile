import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  item: {
    backgroundColor: 'skyblue',
    width: 350,
    padding: 15,
    marginVertical: 8,
    marginHorizontal: 5,
  },
  title: {
    fontSize: 17,
    // marginRight: 100,
    fontWeight: 'bold',
  },
  titleTgl: {
    fontSize: 14,
    // marginRight: 8,
  },
  titleKategori: {
    fontSize: 13,
    // marginRight: 8,
  },
  subtitle: {
    fontSize: 15,
    // marginRight: 250,
    // marginRight: 3,
  },
  titleStatus: {
    fontSize: 14,
    // marginRight: 8,
    fontWeight:'bold',
  },
  imageIcon: {
    width: 22,
    height: 22,
    tintColor: 'white',
  },
  headerView: {
    // flex: 2,
    backgroundColor: '#00bfff',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  imagePassword: {
    paddingTop: 15,
    paddingBottom: 15,
    marginRight: 0,
    marginTop: 8,
    marginBottom: 15,
    alignSelf: 'flex-start',
    width: 30,
    height: 22,
  },
  mainView: {
    flex: 1,
  },
  titleView: {
    // flex: 1,
    backgroundColor: '#00bfff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  addButton: {
    position: 'absolute',
    right: 5,
    bottom: 15,
    backgroundColor: 'yellowgreen',
    width: 60,
    height: 60,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 8,
  },
  addButtonText: {
    color: '#fff',
    fontSize: 11,
  },
  addButtonTextPlus: {
    color: '#fff',
    fontSize: 30,
  },
  imageTitle: {
    // paddingHorizontal: 40,
    // paddingRight: 50,
    // paddingVertical: 100,
    // paddingTop: 15,
    // paddingBottom: 40,
    marginBottom: 5,
    marginTop: 2,
    // marginLeft: 8,
    // marginRight: 230,
    // justifyContent: 'flex-start',
    width: 104,
    height: 45,
    resizeMode:'contain'
  },
  titleText: {
    fontSize: 25,
    paddingTop: 10,
    color: '#fff',
    marginTop: 0,
    marginBottom: 5,
  },
  formView: {
    alignItems: 'flex-start', 
    // justifyContent: 'left',
    flex: 7,
    backgroundColor: 'white',
    marginLeft: 20
  },
  input: {
    justifyContent: 'flex-start',
    borderWidth: 2,
    textAlign: 'center',
    borderColor: '#00bfff',
    borderRadius: 20, 
    paddingTop: 10,
    height: 40,
    width: 300,
    backgroundColor: 'white',
    color: 'black',
    fontSize: 17
  },
  picker: {
    justifyContent: 'flex-start',
    borderBottomWidth: 0.5,
    // borderColor: 'rgb(32, 53, 70)',
    // borderRadius: 20, 
    paddingTop: 10,
    height: 40,
    width: 350,
    // backgroundColor: 'rgb(32, 53, 70)',
    color: 'black',
  },
  label: {
    fontSize: 14, 
    paddingVertical: 7, 
    color: '#00bfff'
  },
  textArea: {
    textAlign: 'left',
    // borderWidth: 2,
    // borderColor: 'rgb(32, 53, 70)',
    // borderRadius: 20,
    borderBottomWidth: 0.5,
    justifyContent: 'flex-start',
    paddingTop: 10,
    // height: 40,
    width: 350,
    backgroundColor: 'white',
    color: 'black',
    fontSize: 15
  },
  buttonView: {
    alignItems: 'center', 
    paddingVertical: 20,
    flex: 2,
    backgroundColor: 'white',
  },
  buttonContainer: {
    backgroundColor: '#90ee90',
    width: 300,
    paddingVertical: 15,
    borderRadius: 20,
  },
  buttonText: {
    textAlign: 'center',
    color: 'rgb(32,53,70)',
    fontWeight: 'bold',
    fontSize: 15,
  },
});
