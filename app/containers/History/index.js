import React from 'react';
import {
  Text, 
  View, 
  Button, 
  Image, 
  TextInput, 
  Picker, 
  ScrollView, 
  TouchableOpacity,
  ToastAndroid,
  FlatList
} from 'react-native';
import styles from './style';
import Api from '../../config/Api';
import LoadingScreen from '../../components/LoadingScreen'
import AsyncStorage from '@react-native-community/async-storage';
import {withNavigation, NavigationEvents} from 'react-navigation';
import moment from 'moment';
import 'moment/locale/id';

type Props = {};

class History extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data:{},
      tickets: [],
      loading: false,
    };
  }

  getData = async () => {
    try {
      const value = await AsyncStorage.getItem("@loginSession");
      
      if (value !== null) {
        valueFix = JSON.parse(value);
        this.setState({ data: valueFix });
        this.getTickets();
      }
      // console.log(this.state);
      // console.log(value);
    } catch (e) {
      // console.log(e);
      // error reading value
    }
  };

  getTickets() {
    // console.log(this.state.data.id)
    let id_user = this.state.data.id;
    Api.get("API/TicketHistory/" + id_user)
      .then(resp => {
        if (resp.httpStatus == 200) {
          this.setState({ tickets: resp.data });
          // console.log(resp.data)
        } else {
          ToastAndroid.show(resp.data.message, ToastAndroid.SHORT);
        }
      })
      .catch(e => {
        // console.log(e);
      });
  }

  toSubmitTicket()
  {
    this.props.navigation.navigate('Ticket');
  }

  componentDidMount(){
    this.getData();
    // this.getTickets();
  }

  static navigationOptions = {
    title: 'My Ticket',
    header: {
      titleStyle: {
        fontSize: 3
      }
    },   
    tabBarIcon: ({tintColor}) => (
      <Image
        source={require('./../../img/icon-ticket.png')}
        style={styles.imageIcon}
      />
    ),
  };

  renderItem(item) {
    let backgroundColor = item.status_ticket == 'open' ? 'lightcoral' 
    : item.status_ticket == 'work_progress' ? 'lightgreen' 
    : item.status_ticket == 'closed' ? 'gold' 
    : 'skyblue'
    return (
      <TouchableOpacity 
        style={
          {
          backgroundColor,
          width: 350,
          padding: 15,
          marginVertical: 8,
          }
        }
        // onPress={ () => this.props.navigation.navigate('Stepper') }
        onPress={ () => this.props.navigation.navigate('Stepper', {item}) }
      >
      <NavigationEvents
        onDidFocus={ () => this.getData()}
      />
        <View style={{ flexDirection: "row", justifyContent: 'space-between' }}>
          <Text style={styles.title}>{item.nama_sub_kategori}</Text>
          <Text style={styles.titleStatus}>Open since :</Text>
        </View>
        <View style={{ flexDirection: "row", justifyContent: 'space-between' }}>
          <Text style={styles.subtitle}></Text>
          <Text style={styles.titleTgl}></Text>
          {/* <Text style={styles.titleStatus}>{status_ticket}</Text> */}
        </View>
        <View style={{ flexDirection: "row", justifyContent: 'space-between' }}>
          <Text style={styles.subtitle}>{item.nama_biro}</Text>
          <Text style={styles.titleTgl}>{moment(item.tanggal_open).format('ddd, DD MMMM YYYY')}</Text>
          {/* <Text style={styles.titleStatus}>{status_ticket}</Text> */}
        </View>
        <View style={{ flexDirection: "row", justifyContent: 'space-between' }}>
          <Text style={styles.titleKategori}>{item.nama_kategori}</Text>
          <Text style={styles.titleKategori}>{item.status_ticket}</Text>
          {/* <Text style={styles.titleStatus}>{status_ticket}</Text> */}
        </View>
      </TouchableOpacity>
    );
  }

  
  render() {
    return (
      <View style={{flex: 1}}>
          <View style={styles.titleView}>
            <Image
              style={styles.imageTitle}
              source={require('./../../img/logo-winner-warna.png')}
            />
            <Text style={styles.titleText}>History Ticket Selesai</Text>
          </View>
      <ScrollView style={styles.mainView}>
        <View style={styles.mainView}>
          <View style={styles.formView}>
            <FlatList
              data={this.state.tickets}
              renderItem={
                ({ item }) => this.renderItem(item)
                }
              keyExtractor={item => item.id_ticket}
            />
          </View> 
 
        </View>
        { this.state.loading && <LoadingScreen /> }
      </ScrollView>
      <TouchableOpacity onPress={() => this.toSubmitTicket()} style={styles.addButton}>
        <Text style={styles.addButtonTextPlus}>+</Text>
      </TouchableOpacity>
      </View>
    )
  }
}

export default withNavigation(History);
