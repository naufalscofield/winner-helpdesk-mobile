import {StyleSheet} from 'react-native';
import { white } from 'ansi-colors';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    flexDirection: 'column',
  },
  viewFooter: {
    marginTop:20,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
  textFooter:{
    // marginBottom: 8,
    marginTop: 20,
    // justifyContent: 'center',
    // alignItems: 'center',
    color: 'black',
    fontWeight:'bold',
    fontSize: 12
  },
  logoContainer: {
    // alignItems: 'center',
    // justifyContent: 'center',
    flex: 1,
    // paddingBottom: 0,
    marginBottom: 50,
  },
  logo: {
    // paddingTop: 1,
    // paddingBottom: 100,
    marginVertical: 50,
    marginHorizontal: 80,
    flex: 1,
    width: null,
    height: null,
    resizeMode: 'contain'
  },
  title: {
    color: '#f7c744',
    fontSize: 25,
    textAlign: 'center',
    marginTop: 5,
    opacity: 0.9,
  },
  infoContainer: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    height: 200,
    marginTop: 25,
    marginBottom: 40,
    paddingBottom: 20,
    paddingTop: 20,
    paddingHorizontal: 20,
    // backgroundColor: 'red',
  },
  input: {
    marginTop: 0,
    marginBottom: 20,
    height: 40,
    backgroundColor: 'transparent',
    color: 'rgb(32,53,70)',
    paddingHorizontal: 10,
    marginBottom: 20,
    borderRadius: 20,
    borderColor: 'rgb(32,53,70)',
    borderWidth: 1,
    fontSize: 17,
    textAlign: 'center'
  },
  buttonContainer: {
    backgroundColor: '#f7c744',
    paddingVertical: 15,
    borderRadius: 20,
  },
  buttonText: {
    textAlign: 'center',
    color: 'rgb(32,53,70)',
    fontWeight: 'bold',
    fontSize: 18,
  },
});