/* eslint-disable eqeqeq */
/* eslint-disable react/no-string-refs */
import React from 'react';

import {
  ToastAndroid,
  Text,
  View,
  Image,
  StatusBar,
  TextInput,
  SafeAreaView,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView
} from 'react-native';


import styles from './style';
import Api from '../../config/Api';

import LoadingScreen from '../../components/LoadingScreen'

import AsyncStorage from '@react-native-community/async-storage';

import { firebase } from '@react-native-firebase/messaging';

import {withNavigation} from 'react-navigation';

type Props = {};

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      nip: '',
      password: '',
      loading: false,
    };
  }


    _login() {
      console.log('aw');
      let params = {
        nip: this.state.nip,
        password: this.state.password,
      };
      
      this.setState({loading: true});
      Api.post('API/login/', params)
      .then(async resp => {
        if (resp.httpStatus == 200) {
          try {
            await AsyncStorage.setItem('@loginSession', JSON.stringify(resp.data.data))
          } catch (e) {
            // saving error
          }
          ToastAndroid.show(resp.data.message, ToastAndroid.SHORT);
          let iduser = resp.data.data.id
          this.updateToken(iduser)
        } else {
          this.setState({loading: false});
          ToastAndroid.show(resp.data.message, ToastAndroid.SHORT);
        }
      })
      .catch(e => {
        console.log(e);
      });
    }

    async updateToken(iduser) {
      console.log('aw');
      const fcmToken = await firebase.messaging().getToken();
      let params = {
        fcm_token: fcmToken,
      };
      
      this.setState({loading: true});
      Api.put('API/token/' + iduser , params)
      .then(async resp => {
        if (resp.httpStatus == 200) {
          this.setState({loading: false});
          this.props.navigation.replace('MainMenu');
        } else {
          this.setState({loading: false});
          ToastAndroid.show(resp.data.message, ToastAndroid.SHORT);
        }
      })
      .catch(e => {
        console.log(e);
      });
    }

    componentDidMount(){
    }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <StatusBar barStyle="light-container" />
        <ScrollView contentContainerStyle={styles.container}>
        <KeyboardAvoidingView behavior="padding" style={styles.container}>
          <View style={styles.container}>
            <View style={styles.logoContainer}>
              <Image
                style={styles.logo}
                source={require('./../../img/logo-winner-login.png')}
              />
            </View>
          </View>
            <View style={styles.infoContainer}>
              <TextInput
                value={this.state.nip}
                onChangeText={value => this.setState({nip: value})}
                style={styles.input}
                placeholder="NIP"
                placeholderTextColor='rgb(32,53,70)'
                returnKeyType="next"
                autoCorrect={false}
                onSubmitEditing={() => this.refs.txtPassword.focus()}
              />
              <TextInput
                value={this.state.password}
                onChangeText={value => this.setState({password: value})}
                style={styles.input}
                placeholder="Password"
                placeholderTextColor='rgb(32,53,70)'
                returnKeyType="go"
                secureTextEntry
                autoCorrect={false}
                ref={'txtPassword'}
              />
              <TouchableOpacity
                style={styles.buttonContainer}
                onPress={() => this._login()}>
                <Text style={styles.buttonText}>LOGIN</Text>
              </TouchableOpacity>
              <View style={styles.viewFooter}>
                <Text style={styles.textFooter}>
                ©Copyright by Wika Industri Energi 2019 - All Right Reserved
                </Text>
              </View>
            </View>
            { this.state.loading && <LoadingScreen /> }
        </KeyboardAvoidingView>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

export default withNavigation(Login);
