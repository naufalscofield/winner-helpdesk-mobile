/* eslint-disable eqeqeq */
/* eslint-disable react/no-string-refs */
import React from 'react';
import * as Animatable from 'react-native-animatable';
import {
  ToastAndroid,
  Text,
  View,
  Image,
  StatusBar,
  TextInput,
  SafeAreaView,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView
} from 'react-native';

import DeviceInfo from 'react-native-device-info';

// or ES6+ destructured imports

import { getUniqueId, getManufacturer } from 'react-native-device-info';

import styles from './style';
import Api from '../../config/Api';

import LoadingScreen from '../../components/LoadingScreen'

import AsyncStorage from '@react-native-community/async-storage';

import { firebase } from '@react-native-firebase/messaging';

import {withNavigation} from 'react-navigation';
import { thisExpression } from '@babel/types';

type Props = {};

class Splash extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      nip: '',
      password: '',
      loading: false,
    };
  }


    async _cekSession(){
      setTimeout( () => {
        this._reset();
      },2500)
    }

    async _reset()
    {
      const value = await AsyncStorage.getItem('@loginSession')
        if (value !== null) {
          this.refs.imgLogo.fadeOutUpBig().then(endState => this.props.navigation.replace('MainMenu'));
          // this.refs.imgLogo.fadeOutUpBig()
          // this.props.navigation.replace('MainMenu');
          // console.log('adaan')
        } else {
          this.refs.imgLogo.fadeOutUpBig().then(endState => this.props.navigation.replace('Login'));
          // this.refs.imgLogo.fadeOutUpBig()
          // this.props.navigation.replace('Login');
        }
    }

    componentDidMount(){
      this._cekSession();
    }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <StatusBar barStyle="light-container" />
        {/* <KeyboardAvoidingView behavior="padding" style={styles.container}> */}
          {/* <View style={styles.container}> */}
            <View style={styles.logoContainer}>
              <Animatable.Image 
                  style={styles.logo}
                  source={require('./../../img/logo-winner-warna.png')} 
                  animation="fadeInUpBig"
                  direction="normal"
                  ref='imgLogo'>
              </Animatable.Image>
                <Text style={styles.textFooter}>
                Version {DeviceInfo.getVersion()}
                </Text>
            </View>
            {/* <View style={styles.viewFooter}> */}
              {/* </View> */}
          {/* </View> */}
        {/* </KeyboardAvoidingView> */}
      </SafeAreaView>
    );
  }
}

export default withNavigation(Splash);
