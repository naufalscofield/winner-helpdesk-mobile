import React from 'react';
import {
  Text, 
  View, 
  Button, 
  Image, 
  TextInput, 
  Picker, 
  ScrollView, 
  TouchableOpacity,
  ToastAndroid,
  FlatList
} from 'react-native';
import styles from './style';
import Api from '../../config/Api';
import LoadingScreen from '../../components/LoadingScreen'
import AsyncStorage from '@react-native-community/async-storage';
import {withNavigation} from 'react-navigation';
import moment from 'moment';
import 'moment/locale/id';
import StepIndicator from 'react-native-step-indicator';
// import moment from 'moment';
import { async } from 'rxjs/internal/scheduler/async';

type Props = {};
class Stepper extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
        data:{},
        tickets: [],
        loading: false,
    };
    this.item = this.props.navigation.getParam('item')
    this.labels = [
        "Open (" + moment(this.item.tanggal_open).format('ddd, DD MMMM YYYY') + ")",
        "Work Progress (" + moment(this.item.tanggal_wp).format('ddd, DD MMMM YYYY') + ")",,
        "Closed (" + moment(this.item.tanggal_close).format('ddd, DD MMMM YYYY') + ")",,
        "Done (" + moment(this.item.tanggal_done).format('ddd, DD MMMM YYYY') + ")",
    ];
    this.customStyles = {
    stepIndicatorSize: 50,
    currentStepIndicatorSize:50,
    separatorStrokeWidth: 5,
    currentStepStrokeWidth: 3,
    stepStrokeCurrentColor: '#00bfff',
    stepStrokeWidth: 3,
    stepStrokeFinishedColor: '#00bfff',
    stepStrokeUnFinishedColor: '#aaaaaa',
    separatorFinishedColor: '#00bfff',
    separatorUnFinishedColor: '#aaaaaa',
    stepIndicatorFinishedColor: '#00bfff',
    stepIndicatorUnFinishedColor: '#aaaaaa',
    stepIndicatorCurrentColor: '#ffffff',
    stepIndicatorLabelFontSize: 18,
    currentStepIndicatorLabelFontSize: 18,
    stepIndicatorLabelCurrentColor: '#00bfff',
    stepIndicatorLabelFinishedColor: '#ffffff',
    stepIndicatorLabelUnFinishedColor: '#ffffff',
    labelColor: '#999999',
    labelSize: 18,
    currentStepLabelColor: '#00bfff',
    }
  }

  getData = async () => {
    try {
      const value = await AsyncStorage.getItem("@loginSession");
      if (value !== null) {
        valueFix = JSON.parse(value);
        this.setState({ data: valueFix });
      }
      console.log(value);
    } catch (e) {
      console.log(e);
      // error reading value
    }
  };


  toSubmitTicket()
  {
    this.props.navigation.navigate('Ticket');
  }

  componentDidMount(){
    this.getData();
  }

  static navigationOptions = {
    title: 'My Ticket',
    header: {
      titleStyle: {
        fontSize: 3
      }
    },   
    tabBarIcon: ({tintColor}) => (
      <Image
        source={require('./../../img/icon-ticket.png')}
        style={styles.imageIcon}
      />
    ),
  };

  
  render() {
   
   let currentState = this.item.status_ticket == 'open' ?  0
        : this.item.status_ticket == 'work_progress' ?  1 
        : this.item.status_ticket == 'closed' ?  2 
        :  3
  
    return(
        <View style={{flex: 1}}>
          <View style={styles.titleView}>
            <Image
              style={styles.imageTitle}
              source={require('./../../img/logo-winner-warna.png')}
            />
            <Text style={styles.titleText}>Timeline Ticket</Text>
          </View>
        <View style={styles.mainStep}>
            <StepIndicator
            stepCount={4}
            customStyles={this.customStyles}
            currentPosition={currentState}
            labels={this.labels}
            direction="vertical"
            />
        </View>

      <ScrollView style={styles.mainView}>
        <View style={styles.viewImage}>
            <Image
              style={styles.timelineImage}
              source={require('./../../img/timeline.png')}
            />
          </View>
      </ScrollView>
      </View>
    )
  }

  onPageChange(position){
    this.setState({currentStep: position});
}
}

export default withNavigation(Stepper);
