import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  item: {
    backgroundColor: 'skyblue',
    width: 350,
    padding: 15,
    marginVertical: 8,
    marginHorizontal: 5,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  titleTgl: {
    fontSize: 12,
  },
  subtitle: {
    fontSize: 15,
  },
  titleStatus: {
    fontSize: 14,
    fontWeight:'bold',
  },
  imageIcon: {
    width: 22,
    height: 22,
    tintColor: 'white',
  },
  mainView: {
    flex: 1,
  },
  mainStep: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleView: {
    backgroundColor: '#00bfff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  addButton: {
    position: 'absolute',
    right: 5,
    bottom: 15,
    backgroundColor: 'yellowgreen',
    width: 60,
    height: 60,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 8,
  },
  addButtonText: {
    color: '#fff',
    fontSize: 11,
  },
  addButtonTextPlus: {
    color: '#fff',
    fontSize: 30,
  },
  imageTitle: {
    marginBottom: 5,
    marginTop: 2,
    width: 104,
    height: 45,
    resizeMode:'contain'
  },
  timelineImage: {
    marginBottom: 1,
    // justifyContent: 'center',
    // marginTop: 2,
    width: 500,
    height: 200,
    resizeMode:'contain'
  },
  viewImage: {
    paddingBottom: 100,
    justifyContent: 'center',
    alignItems: 'center',
    // marginTop: 300,
    // backgroundColor: 'red',
    // flex: 1
    // height: 200,
    // resizeMode:'contain'
  },
  titleText: {
    fontSize: 25,
    paddingTop: 10,
    color: '#fff',
    marginTop: 0,
    marginBottom: 5,
  },
  formView: {
    alignItems: 'flex-start', 
    flex: 7,
    backgroundColor: 'white',
    marginLeft: 20
  },
  input: {
    justifyContent: 'flex-start',
    borderWidth: 2,
    textAlign: 'center',
    borderColor: '#00bfff',
    borderRadius: 20, 
    paddingTop: 10,
    height: 40,
    width: 300,
    backgroundColor: 'white',
    color: 'black',
    fontSize: 17
  },
  picker: {
    justifyContent: 'flex-start',
    borderBottomWidth: 0.5,
    paddingTop: 10,
    height: 40,
    width: 350,
    color: 'black',
  },
  label: {
    fontSize: 14, 
    paddingVertical: 7, 
    color: '#00bfff'
  },
  textArea: {
    textAlign: 'left',
    borderBottomWidth: 0.5,
    justifyContent: 'flex-start',
    paddingTop: 10,
    width: 350,
    backgroundColor: 'white',
    color: 'black',
    fontSize: 15
  },
  buttonView: {
    alignItems: 'center', 
    paddingVertical: 20,
    flex: 2,
    backgroundColor: 'white',
  },
  buttonContainer: {
    backgroundColor: '#90ee90',
    width: 300,
    paddingVertical: 15,
    borderRadius: 20,
  },
  buttonText: {
    textAlign: 'center',
    color: 'rgb(32,53,70)',
    fontWeight: 'bold',
    fontSize: 15,
  },
});
