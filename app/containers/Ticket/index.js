import React from 'react';
import {Text, View, Button, Image, TextInput, Picker, ScrollView, TouchableOpacity, ToastAndroid} from 'react-native';
import styles from './style';
import Api from '../../config/Api';
import LoadingScreen from '../../components/LoadingScreen'
import AsyncStorage from '@react-native-community/async-storage';
import {withNavigation} from 'react-navigation';
import { async } from 'rxjs/internal/scheduler/async';

type Props = {};
class Ticket extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data:{},
      dataBiro:[],
      dataKategori:[],
      dataSubKategori:[],
      dataInventaris:[],
      loading: false,
      selectedBiro:null,
      selectedKategori:null,
      selectedSubKategori:null,
      selectedPriority:null,
      selectedInventaris:null,
      pesan:null
    };
  }

   _submitTicket() {
     if ( (this.state.selectedBiro == null) || (this.state.selectedKategori == null) || (this.state.selectedPriority == null) || (this.state.pesan == null) ){
      ToastAndroid.show('Harap lengkapi form!', ToastAndroid.SHORT);
     } else {
    let params = {
      id_user         : this.state.data.id,
      id_biro         : this.state.selectedBiro,
      id_kategori     : this.state.selectedKategori,
      id_sub_kategori     : this.state.selectedSubKategori,
      priority        : this.state.selectedPriority,
      no_inventaris        : this.state.selectedInventaris,
      pesan           : this.state.pesan
    };

    console.log(params);

    this.setState({loading: true});
    Api.post('API/TicketsUsers/', params)
      .then(resp => {
        if (resp.httpStatus == 200) {
          this.setState({loading: false});
          ToastAndroid.show('Submit Ticket Success!', ToastAndroid.SHORT);
          this.props.navigation.navigate('MyTicket')
        } else {
          this.setState({loading: false});
          ToastAndroid.show('Submit Ticket Failed!', ToastAndroid.SHORT);
        }
      })
      .catch(e => {
        console.log(e);
      });
      
    }
  }

  getData = async () => {
    try {
      const value = await AsyncStorage.getItem('@loginSession')
      if(value !== null) {
        valueFix = JSON.parse(value);
          this.setState({data:valueFix});  
      } 
      console.log(value);
    } catch(e) {
      console.log(e);
      // error reading value
    }
  }

  getBiro() {
    Api.get('API/Biro/')
      .then(resp => {
        if (resp.httpStatus == 200) {
          console.log(resp.data.data)
          this.setState({dataBiro: resp.data.data});
        } else {
          ToastAndroid.show(resp.data.message, ToastAndroid.SHORT);
        }
  })
  .catch(e => {
    console.log(e);
  });
  
}
  
getKategori(id_biro) {
    this.setState({loading: true});
    Api.get('API/Kategori/' + id_biro)
      .then(resp => {
        if (resp.httpStatus == 200) {
          this.setState({loading: false});
          console.log(resp.data.data)
          this.setState({dataKategori: resp.data.data});
        } else {
          this.setState({loading: false});
          ToastAndroid.show(resp.data.message, ToastAndroid.SHORT);
        }
  })
  .catch(e => {
    console.log(e);
  });
  
}

getInventaris() {
    this.setState({loading: true});
    Api.get('API/Inventaris/')
      .then(resp => {
        if (resp.httpStatus == 200) {
          this.setState({loading: false});
          console.log(resp.data.data)
          this.setState({dataInventaris: resp.data.data});
        } else {
          this.setState({loading: false});
          ToastAndroid.show(resp.data.message, ToastAndroid.SHORT);
        }
  })
  .catch(e => {
    console.log(e);
  });
  
}

getSubKategori(id_kategori) {
    this.setState({loading: true});
    Api.get('API/SubKategori/' + id_kategori)
      .then(resp => {
        if (resp.httpStatus == 200) {
          this.setState({loading: false});
          console.log(resp.data.data)
          this.setState({dataSubKategori: resp.data.data});
        } else {
          this.setState({loading: false});
          ToastAndroid.show(resp.data.message, ToastAndroid.SHORT);
        }
  })
  .catch(e => {
    console.log(e);
  });
  
}

  componentDidMount(){
    this.getData();
    this.getBiro();
    this.getInventaris();
  }

  static navigationOptions = {
    title: 'Submit Ticket',
    tabBarIcon: ({tintColor}) => (
      <Image
        source={require('./../../img/icon-ticket.png')}
        style={styles.imageIcon}
      />
    ),
  };

  render() {
    return (
      <ScrollView style={styles.mainView}>
        <View style={styles.mainView}>
          <View style={styles.titleView}>
            <Image
              style={styles.imageTitle}
              source={require('./../../img/logo-winner-warna.png')}
            />
            <Text style={styles.titleText}>Submit Ticket</Text>
          </View>
          <View style={styles.formView}>

            <Text style={styles.label}>
            Pilih Biro
            </Text>
            <Picker
              selectedValue={this.state.selectedBiro}
              onValueChange={async (itemValue, itemIndex) => {
                await this.setState({selectedBiro: itemValue, selectedKategori: null})
                this.getKategori(itemValue)
                }
              }
              style={styles.picker}
              ref={'txtBiro'}
              onSubmitEditing={() => this.refs.txtKategori.focus()}
              >
              <Picker.Item label="-- PILIH BIRO --" value="" />
                {
                  this.state.dataBiro.map((data, index) => {
                  return <Picker.Item label={data.nama_biro} value={data.id} key={'key'+index} />
                  })
                }
            </Picker>

            <Text style={styles.label}>
            Pilih Kategori
            </Text>
              <Picker
                selectedValue={this.state.selectedKategori}
                onValueChange={async (itemValue, itemIndex) => 
                  {
                    await this.setState({selectedKategori: itemValue})
                    this.getSubKategori(itemValue)
                  }
                }
                style={styles.picker}
                ref={'txtKategori'}
                onSubmitEditing={() => this.refs.txtSubKategori.focus()}
                >
                  <Picker.Item label="-- PILIH KATEGORI --" value="" />
                  {
                    this.state.dataKategori.map((data, index) => {
                    return <Picker.Item label={data.nama_kategori} value={data.id} key={'key'+index} />
                    })
                  }
              </Picker>

            <Text style={styles.label}>
            Pilih Sub Kategori
            </Text>
            <Picker
              selectedValue={this.state.selectedSubKategori}
              onValueChange={(itemValue, itemIndex) => {
                this.setState({selectedSubKategori: itemValue})
                }
              }
              style={styles.picker}
              ref={'txtSubKategori'}
              onSubmitEditing={() => this.refs.txtPriority.focus()}
              >
                <Picker.Item label="-- PILIH SUB KATEGORI --" value="" />
                {
                  this.state.dataSubKategori.map((data, index) => {
                  return <Picker.Item label={data.nama_sub_kategori} value={data.id} key={'key'+index} />
                  })
                }
            </Picker>

            <Text style={styles.label}>
            Pilih Inventaris
            </Text>
            <Picker
              selectedValue={this.state.selectedInventaris}
              onValueChange={(itemValue, itemIndex) => {
                this.setState({selectedInventaris: itemValue})
                }
              }
              style={styles.picker}
              ref={'txtPriority'}
              onSubmitEditing={() => this.refs.txtPriority.focus()}
              >
                <Picker.Item label="-- PILIH INVENTARIS --" value="" />
                {
                  this.state.dataInventaris.map((data, index) => {
                  return <Picker.Item label={data.no_inventaris} value={data.no_inventaris} key={'key'+index} />
                  })
                }
            </Picker>
            
            <Text style={styles.label}>
            Pilih Priority
            </Text>
            <Picker
              selectedValue={this.state.selectedPriority}
              onValueChange={(itemValue, itemIndex) =>
                this.setState({ selectedPriority: itemValue })
              }
              style={styles.picker}
              ref={'txtPriority'}
              onSubmitEditing={() => this.refs.txtPesan.focus()}
              >
              <Picker.Item label="-- PILIH PRIORITY --" value="" />
              <Picker.Item label="LOW" value="low" />
              <Picker.Item label="NORMAL" value="normal" />
              <Picker.Item label="URGENT" value="urgent" />
            </Picker>

            <Text style={styles.label}>
            Pesan
            </Text>
            <TextInput
              value={this.state.pesan}
              onChangeText={value => this.setState({ pesan:value })}
              style={styles.textArea}
              multiline
              numberOfLines={3}
              placeholder="Masukan detail pesan"
              placeholderTextColor="lightsteelblue"
              returnKeyType="next"
              autoCorrect={false}
              ref={'txtPesan'}
            />
            
          </View> 
          <View
            style={styles.buttonView}
          >
            <TouchableOpacity
                style={styles.buttonContainer}
                onPress={() => this._submitTicket()}
                >
                <Text style={styles.buttonText}>SUBMIT TICKET</Text>
              </TouchableOpacity>
          </View> 
        </View>
        { this.state.loading && <LoadingScreen /> }
      </ScrollView>
    );
  }
}

export default withNavigation(Ticket);
