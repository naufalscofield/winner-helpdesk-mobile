import React from 'react';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
// import {createMaterialTopTabNavigator} from 'react-navigation-tabs';
import {createBottomTabNavigator} from 'react-navigation-tabs';

import Login from './app/containers/Login';
import Splash from './app/containers/Splash';
import Profile from './app/containers/Profile';
import Ticket from './app/containers/Ticket';
import History from './app/containers/History';
import Password from './app/containers/Password';
import Logout from './app/containers/Logout';
import MyTicket from './app/containers/MyTicket';
import Stepper from './app/containers/Stepper';
import MainTab from './app/components/MainTab';

// const MainMenuScreen = createMaterialTopTabNavigator(
const MainMenuScreen = createBottomTabNavigator(

  {
    Profile: Profile,
    MyTicket: MyTicket,
    Logout: Logout,
  },
  
  {
    swipeEnabled: true,
    tabBarComponent: props => <MainTab {...props}/>
  },
);

const AppNavigator = createStackNavigator(
  {
    Splash: Splash,
    Login: Login,
    MainMenu: MainMenuScreen,
    Password: Password,
    Ticket : Ticket,
    History : History,
    Stepper : Stepper
  },
  {
    initialRouteName: 'Splash',
    headerMode: 'none',
  },
);

export default createAppContainer(AppNavigator);
